const knex = require('knex');
const { knexSnakeCaseMappers } = require('objection');
const dbConfig = require('../knexfile');

module.exports = () => {
  const env = process.env.NODE_ENV || 'development';
  let conf = dbConfig[env];
  conf = {
    ...conf,
    ...knexSnakeCaseMappers(),
  };
  const db = knex(conf);
  return db;
};
