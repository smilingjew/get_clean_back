const sqlite3 = require('sqlite3');

exports.isDBError = error => error.errno === sqlite3.CONSTRAINT;
