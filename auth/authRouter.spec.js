/* global describe it before after afterEach */
const chai = require('chai');
const request = require('supertest');
const session = require('supertest-session');
const { addHours } = require('date-fns');
const { getTestDatabase } = require('../test');
const getOrm = require('../models');
const createServer = require('../server');

const { expect } = chai;

let db, app, orm;

const testHash = '$2a$10$d/nDvbZtjtQZtW5onQD.puO3Nk1DPs89u63iA3eAad8Xba/HEo522';

describe('Test auth routes', async () => {
  before(async () => {
    db = await getTestDatabase();
    orm = getOrm(db);
    app = createServer({
      db,
      orm,
    });
  });

  it('Should be invalid when nothing is sent', async () => {
    let resp = await request(app)
      .post('/auth/login')
      .send({});
    expect(resp.status).to.equal(401);
    expect(resp.body).to.deep.equal({
      error: 'Invalid Email/Password',
    });

    resp = await request(app)
      .post('/auth/login')
      .send({
        'email': 'test',
      });
    expect(resp.status).to.equal(401);
    expect(resp.body).to.deep.equal({
      error: 'Invalid Email/Password',
    });

    resp = await request(app)
      .post('/auth/login')
      .send({
        'email': '',
        'password': '',
      });
    expect(resp.status).to.equal(401);
    expect(resp.body).to.deep.equal({
      error: 'Invalid Email/Password',
    });

    resp = await request(app)
      .post('/auth/login')
      .send({
        'email': '',
        'password': 'test',
      });
    expect(resp.status).to.equal(401);
    expect(resp.body).to.deep.equal({
      error: 'Invalid Email/Password',
    });

    resp = await request(app)
      .post('/auth/login')
      .send({
        'email': '   ',
        'password': '   ',
      });
    expect(resp.status).to.equal(401);
    expect(resp.body).to.deep.equal({
      error: 'Invalid Email/Password',
    });
  });

  it('should be invalid when no user matches', async () => {
    let resp = await request(app)
      .post('/auth/login')
      .send({
        'email': 'test@example1.com',
        'password': 'test',
      });
    expect(resp.status).to.equal(401);
    expect(resp.body).to.deep.equal({
      error: 'Invalid Email/Password',
    });

    await orm.User.query().insert({
      email: 'test@example.com',
      password: testHash,
    });

    resp = await request(app)
      .post('/auth/login')
      .send({
        'email': 'test@example1.com',
        'password': 'test',
      });
    expect(resp.status).to.equal(401);
    expect(resp.body).to.deep.equal({
      error: 'Invalid Email/Password',
    });

    resp = await request(app)
      .post('/auth/login')
      .send({
        'email': 'test@example.com',
        'password': 'test',
      });
    expect(resp.status).to.equal(401);
    expect(resp.body).to.deep.equal({
      error: 'Invalid Email/Password',
    });

    resp = await request(app)
      .post('/auth/login')
      .send({
        'email': 'test@example.com',
        'password': 'testpass',
      });
    expect(resp.status).to.equal(200);

    await orm.User.query().delete();
  });

  it('should return a user with an auth token when valid', async () =>{
    let tokenCount = await orm.UserToken.query().count('* as all').first();
    expect(tokenCount.all).to.equal(0);
    const user = await orm.User.query().insert({
      email: 'test@example.com',
      password: testHash,
    });
    const now = new Date();
    const resp = await request(app)
      .post('/auth/login')
      .send({
        'email': 'test@example.com',
        'password': 'testpass',
      });
    expect(resp.status).to.equal(200);
    tokenCount = await orm.UserToken.query().count('* as all').first();
    const token = await orm.UserToken.query().first();
    const oneHour = addHours(now, 1);
    expect(token.expiration).to.be.greaterThan(oneHour.getTime());
    expect(token.expiration).to.be.lessThan(addHours(new Date(), 1).getTime());
    expect(tokenCount.all).to.equal(1);
    expect(resp.body).to.deep.equal({
      id: user.id,
      email: 'test@example.com',
      token: token.token,
    });
  });

  it('should not save id to session if invalid login', async () => {
    await orm.User.query().insert({
      email: 'test@example.com',
      password: testHash,
    });
    const requestor = session(app);
    await requestor
      .post('/auth/login')
      .send({
        'email': 'test@example.com',
        'password': 'bad',
      });
    const resp = await requestor.get('/auth/user');
    expect(resp.status).to.equal(401);
  });

  it('should save id to session if valid login', async () => {
    await orm.User.query().insert({
      email: 'test@example.com',
      password: testHash,
    });
    const requestor = session(app);
    await requestor
      .post('/auth/login')
      .send({
        'email': 'test@example.com',
        'password': 'testpass',
      });
    const resp = await requestor.get('/auth/user');
    expect(resp.status).to.equal(200);
  });

  it('should create a new token', async () => {
    let tokenCount = await orm.UserToken.query().count('* as all').first();
    expect(tokenCount.all).to.equal(0);
    await orm.User.query().insert({
      email: 'test@example.com',
      password: testHash,
    });
    const requestor = session(app);
    await requestor
      .post('/auth/login')
      .send({
        'email': 'test@example.com',
        'password': 'testpass',
      });
    const resp = await requestor.get('/auth/user');
    expect(resp.status).to.equal(200);

    tokenCount = await orm.UserToken.query().count('* as all').first();
    expect(tokenCount.all).to.equal(1);
    const token = await orm.UserToken.query().first();
    expect(resp.body.token).to.equal(token.token);
  });

  it('should use unexpired token', async () => {
    const user = await orm.User.query().insert({
      email: 'test@example.com',
      password: testHash,
    });
    const token = await user.$relatedQuery('tokens').insert({});
    const requestor = session(app);
    await requestor
      .post('/auth/login')
      .send({
        'email': 'test@example.com',
        'password': 'testpass',
      });
    const resp = await requestor.get('/auth/user');
    expect(resp.status).to.equal(200);

    const tokenCount = await orm.UserToken.query().count('* as all').first();
    expect(tokenCount.all).to.equal(1);
    expect(resp.body.token).to.equal(token.token);
  });

  it('should use ignore expired token', async () => {
    const user = await orm.User.query().insert({
      email: 'test@example.com',
      password: testHash,
    });
    const token = await user.$relatedQuery('tokens').insert({
      token: 'expiredtoken',
      expiration: new Date(2017, 2, 1),
    });

    const requestor = session(app);
    await requestor
      .post('/auth/login')
      .send({
        'email': 'test@example.com',
        'password': 'testpass',
      });
    const resp = await requestor.get('/auth/user');
    expect(resp.status).to.equal(200);

    const tokenCount = await orm.UserToken.query().count('* as all').first();
    expect(tokenCount.all).to.equal(2);
    expect(resp.body.token).to.not.equal(token.token);
  });

  it('should return user data', async () => {
    const user = await orm.User.query().insert({
      email: 'test@example.com',
      password: testHash,
    });
    const requestor = session(app);
    await requestor
      .post('/auth/login')
      .send({
        'email': 'test@example.com',
        'password': 'testpass',
      });
    const resp = await requestor.get('/auth/user');
    expect(resp.status).to.equal(200);
    expect(resp.body.id).to.equal(user.id);
    expect(resp.body.email).to.equal(user.email);
    expect(resp.body.password).to.be.undefined;
  });

  it('should return errors when registering user', async () => {
    let userCount = await orm.User.query().count('* as all').first();
    expect(userCount.all).to.equal(0);
    let resp = await request(app)
      .post('/auth/register')
      .send({});

    userCount = await orm.User.query().count('* as all').first();
    expect(userCount.all).to.equal(0);

    expect(resp.status).to.equal(422);

    expect(resp.body).to.deep.equal({
      error: {
        confirm: ['Confirm can\'t be blank'],
        email: ['Email can\'t be blank'],
        password: ['Password can\'t be blank'],
      },
    });

    resp = await request(app)
      .post('/auth/register')
      .send({
        email: 'test@example.com',
        password: 'test',
        confirm: 'otherTest',
      });

    userCount = await orm.User.query().count('* as all').first();
    expect(userCount.all).to.equal(0);

    expect(resp.status).to.equal(422);

    expect(resp.body).to.deep.equal({
      error: {
        confirm: ['Confirm is not equal to password'],
      },
    });
  });

  it('should create user', async () => {
    let userCount = await orm.User.query().count('* as all').first();
    expect(userCount.all).to.equal(0);
    const resp = await request(app)
      .post('/auth/register')
      .send({
        email: 'test@example.com',
        password: 'test',
        confirm: 'test',
      });

    expect(resp.status).to.equal(201);

    userCount = await orm.User.query().count('* as all').first();
    expect(userCount.all).to.equal(1);
    const user = await orm.User.query().first();
    const token = await user.getToken();
    expect(resp.body).to.deep.equal({
      token,
      id: user.id,
      email: 'test@example.com',
    });
  });

  it('should not create duplicate email', async () => {
    await orm.User.query().insert({
      email: 'test@example.com',
      password: testHash,
    });
    const resp = await request(app)
      .post('/auth/register')
      .send({
        email: 'test@example.com',
        password: 'test',
        confirm: 'test',
      });
    expect(resp.status).to.equal(422);
    expect(resp.body).to.deep.equal({
      error: {
        email: ['A user with that email already exists'],
      },
    });
  });

  afterEach(async () => {
    await orm.UserToken.query().delete();
    await orm.User.query().delete();
  });

  after(async () => {
    db.destroy();
  });
});
