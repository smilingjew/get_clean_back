const { Router }= require('express');
const validate = require('validate.js');
const router = Router();


const registerConstraints = {
  email: {
    presence: {
      allowEmpty: false,
    },
  },
  password: {
    presence: {
      allowEmpty: false,
    },
  },
  confirm: {
    presence: {
      allowEmpty: false,
    },
    equality: 'password',
  },
};


router.post('/login', async (req, res) => {
  const { User } = req.app.orm;
  const errors = validate(req.body, User.constraints);
  req.session.uid = null;
  if(errors) {
    res.status(401).send({
      error: 'Invalid Email/Password',
    });
    return;
  }
  const { email, password } = req.body;
  const user = await User.query().where('email', '=', email).first();
  if(!user) {
    res.status(401).send({
      error: 'Invalid Email/Password',
    });
    return;
  }
  const valid = await user.checkPassword(password);
  if(!valid) {
    res.status(401).send({
      error: 'Invalid Email/Password',
    });
    return;
  }
  const token = await user.getToken();
  user.token = token;
  req.session.uid = user.id;
  res.send(user);
});

router.post('/register', async (req, res) => {
  const errors = validate(req.body, registerConstraints);
  if (errors) {
    res.status(422).send({
      error: errors,
    });
    return;
  }

  const { User } = req.app.orm;
  const existing = await User.query().where({
    email: req.body.email,
  }).first();
  if (existing) {
    res.status(422).send({
      error: {
        email: ['A user with that email already exists'],
      },
    });
    return;
  }
  const password = await User.hashPassword(req.body.password);
  const user = await User.query().insert({
    password,
    email: req.body.email,
  });
  user.token = await user.getToken();
  req.session.uid = user.id;
  res.status(201).send(user);
});

router.get('/user', async (req, res) => {
  const userId = req.session.uid;
  if (!userId || userId === 'null') {
    res.status(401).send({
      error: 'not logged in',
    });
    return;
  }
  const { User } = req.app.orm;
  const user = await User.query().where({ id: userId }).first();
  if (!user) {
    res.status(401).send({
      error: 'not logged in',
    });
    return;
  }
  const token = await user.getToken();
  user.token = token;
  res.send(user);
});

exports.router = router;

exports.authWrap = async (req, res, next) => {
  const { User } = req.app.orm;
  const user = await User.getUserFromAuth(req.headers.authorization);
  if (!user) {
    res.status(403).send({ error: 'Unauthorized' });
    return;
  }
  req.user = user;
  next();
};
