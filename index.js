require('dotenv').config();
const createServer = require('./server');
const createDatabase = require('./database');
const createOrm = require('./models');

const db = createDatabase();
const orm = createOrm(db);

const app = createServer({
  db,
  orm,
});

app.listen(8080, () => {
  /* eslint-disable no-console */
  console.log('Server listening on 8080...');
  /* eslint-enable no-console */
});
