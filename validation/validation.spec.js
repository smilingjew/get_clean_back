/* global describe it */
require('./index');
const validation = require('validate.js');
const chai = require('chai');

const { expect } = chai;

describe('Validation date time parse tests', async () => {
  it('should not accept invalid date', async () => {
    let err = validation.validate({
      foo: '',
    }, {
      foo: {
        datetime: true,
      },
    });
    expect(err).to.deep.equal({
      foo: ['Foo must be a valid date'],
    });

    err = validation.validate({
      foo: 'some other string',
    }, {
      foo: {
        datetime: true,
      },
    });
    expect(err).to.deep.equal({
      foo: ['Foo must be a valid date'],
    });
  });

  it('should accept valid date', async () => {
    let err = validation.validate({
      foo: '2017-05-01',
    }, {
      foo: {
        datetime: true,
      },
    });
    expect(err).to.be.undefined;

    err = validation.validate({
      foo: 20001,
    }, {
      foo: {
        datetime: true,
      },
    });
    expect(err).to.be.undefined;
  });
});
