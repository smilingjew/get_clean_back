const validate = require('validate.js');
const { parse, format } = require('date-fns');
const { Validator } = require('objection');

validate.extend(validate.validators.datetime, {
  parse: (value) => {
    return parse(value);
  },
  format: (value) => {
    return format(new Date(value), 'GGGG-MM-DDTHH:mm:ssZ');
  },
});

exports.Validator = class GCValidator extends Validator {
  validate({ model, json }) {
    const modelClass = model.constructor;
    const constraints = modelClass.constraints;
    if (!constraints) {
      return json;
    }
    const errors = validate(json, constraints);

    if(errors) {
      throw new exports.ValidationError(errors);
    }
    return json;
  }
};

exports.ValidationError = class ValidationError extends Error {
  constructor(errors) {
    super('Validation Error');
    this.errors = errors;
  }
};

