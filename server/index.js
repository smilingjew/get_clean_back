require('express-async-errors');
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const session = require('express-session');
const { ValidationError } = require('../validation');

const loadMiddlewares = app => {
  const secret = process.env.SECRET_KEY || 'test secret';
  app.use(cors({ credentials: true, origin: true }));
  app.use(session({
    secret,
    saveUninitialized: true,
    resave: false,
    cookie: { secure: false },
  }));
  app.use(bodyParser.json());
  app.use(async (req, res, next) => {
    const start = new Date().getTime();
    if(process.env.NODE_ENV !== 'test') {
      res.on('finish', () => {
        const total = (new Date().getTime() - start) / 1000;
        /* eslint-disable no-console */
        console.log(`${req.method} ${req.originalUrl} ${res.statusCode} - ${total}s`);
        /* eslint-enable no-console */
      });
    }
    await next();
  });
};

const loadRoutes = app => {
  const { router: authRouter } = require('../auth');
  const { router: apiRouter } = require('../api');
  app.use('/auth', authRouter);
  app.use('/api', apiRouter);
};

const loadErrorHandlers = app => {
  app.use(async (err, req, res, next) => {
    if(err instanceof ValidationError) {
      res.status(422).send({ error: err.errors });
    }
    else {
      next(err);
    }
  });

  app.use(async (err, req, res) => {
    res.status(500).send({ error: 'server error' });
  });
};


module.exports = ({ db, orm }) => {
  const app = express();
  app.db = db;
  app.orm = orm;
  loadMiddlewares(app);

  loadRoutes(app);

  loadErrorHandlers(app);

  return app;
};
