const validate = require('validate.js');
const { Router } = require('express');
const { isDBError } = require('../database/utils');
const { authWrap } = require('../auth');
const { ValidationError } = require('../validation');

const router = Router();

router.use(authWrap);

router.param('choreId', async (req, res, next, choreId) => {
  const chore = await req.user.$relatedQuery('chores')
    .where({ id: choreId }).first();
  if(!chore) {
    res.status(404).send({ error: 'not found' });
  }
  else {
    req.chore = chore;
    next();
  }
});

router.param('instanceId', async (req, res, next, instanceId) => {
  const instance = await req.user.$relatedQuery('choreInstances')
    .where({ id: instanceId }).first();
  if(!instance) {
    res.status(404).send({ error: 'not found' });
  }
  else {
    req.instance = instance;
    next();
  }
});

router
  .route('/chores')
  .get(async (req, res) => {
    const chores = await req.user.$relatedQuery('chores');
    res.send(chores);
  })
  .post(async (req, res) => {
    let chore;
    try {
      chore = await req.user.$relatedQuery('chores')
        .insert(req.body);
    }
    catch (error) {
      if(!isDBError(error)) {
        throw error;
      }
      res.status(422).send({
        error: {
          name: ['A chore with that name exists'],
        },
      });
    }
    res.status(201).send(chore);
  });

router.route('/chores/:choreId')
  .get(async (req, res) => {
    res.send(req.chore);
  })
  .post(async (req, res) => {
    let chore;
    try {
      chore = await req.user.$relatedQuery('chores')
        .updateAndFetchById(req.chore.id, req.body);
    }
    catch (error) {
      if(!isDBError(error)) {
        throw error;
      }
      res.status(422).send({
        error: {
          name: ['A chore with that name exists'],
        },
      });
    }
    res.status(201).send(chore);
  })
  .delete(async (req, res) => {
    await req.user.$relatedQuery('chores')
      .delete()
      .where({ id: req.chore.id });
    await req.user.$relatedQuery('choreInstances')
      .delete()
      .where({ choreId: req.chore.id, completed: false });
    res.status(204).send();
  });

router.route('/chore-instances')
  .get(async (req, res) => {
    const instances = await req.user.$relatedQuery('choreInstances')
      .where({ completed: false })
      .orderBy('dueDate', 'desc');
    res.send(instances);
  });

router.route('/chore-instances/:instanceId')
  .get(async (req, res) => {
    res.send(req.instance);
  });

router.route('/chore-completions/:instanceId')
  .post(async (req, res) => {
    const errors = validate.validate(req.body, {
      completedDate: {
        datetime: true,
        presence: {
          allowEmpty: false,
        },
      },
    });
    if (errors) {
      throw new ValidationError(errors);
    }
    const { completedDate } = req.body;
    await req.instance.completeInstance(completedDate);
    res.send(req.instance);
  });

exports.router = router;
