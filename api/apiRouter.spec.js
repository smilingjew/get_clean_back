/* global describe it before after afterEach */
const chai = require('chai');
const { isWithinRange, addDays } = require('date-fns');
const request = require('supertest');
const { getTestDatabase } = require('../test');
const getOrm = require('../models');
const createServer = require('../server');

const { expect } = chai;

let db, app, orm, token, user;


describe('Test api routes', async () => {
  before(async () => {
    db = await getTestDatabase();
    orm = getOrm(db);
    app = createServer({
      db,
      orm,
    });

    user = await orm.User.query().insert({
      email: 'test@example.com',
      password: 'whocares',
    });
    token = await user.getToken();
  });

  it('does not create chore when bad data is sent', async () => {
    let count = await orm.Chore.query().count('* as all').first();
    expect(count.all).to.equal(0);
    const resp = await request(app)
      .post('/api/chores')
      .set('Authorization', `Bearer ${token}`)
      .send({});
    expect(resp.status).to.equal(422);
    expect(resp.body.error).to.not.equal(undefined);
    count = await orm.Chore.query().count('* as all').first();
    expect(count.all).to.equal(0);
  });

  it('does not accept invalid name', async () => {
    let resp = await request(app)
      .post('/api/chores')
      .set('Authorization', `Bearer ${token}`)
      .send({});
    expect(resp.status).to.equal(422);
    expect(resp.body.error.name).to.not.equal(undefined);
    let nameError = resp.body.error.name;
    expect(nameError).to.deep.equal([
      'Name can\'t be blank',
    ]);

    resp = await request(app)
      .post('/api/chores')
      .set('Authorization', `Bearer ${token}`)
      .send({
        name: null,
      });
    expect(resp.status).to.equal(422);
    expect(resp.body.error.name).to.not.equal(undefined);
    nameError = resp.body.error.name;
    expect(nameError).to.deep.equal([
      'Name can\'t be blank',
    ]);

    resp = await request(app)
      .post('/api/chores')
      .set('Authorization', `Bearer ${token}`)
      .send({
        name: '',
      });
    expect(resp.status).to.equal(422);
    expect(resp.body.error.name).to.not.equal(undefined);
    nameError = resp.body.error.name;
    expect(nameError).to.deep.equal([
      'Name can\'t be blank',
    ]);

    resp = await request(app)
      .post('/api/chores')
      .set('Authorization', `Bearer ${token}`)
      .send({
        name: '   ',
      });
    expect(resp.status).to.equal(422);
    expect(resp.body.error.name).to.not.equal(undefined);
    nameError = resp.body.error.name;
    expect(nameError).to.deep.equal([
      'Name can\'t be blank',
    ]);

    resp = await request(app)
      .post('/api/chores')
      .set('Authorization', `Bearer ${token}`)
      .send({
        name: 'test name',
      });
    expect(resp.status).to.equal(422);
    expect(resp.body.error.name).to.equal(undefined);
  });

  it('does not accept invalid frequency', async () => {
    let resp = await request(app)
      .post('/api/chores')
      .set('Authorization', `Bearer ${token}`)
      .send({});
    expect(resp.status).to.equal(422);
    expect(resp.body.error.frequency).to.not.equal(undefined);
    let frequencyError = resp.body.error.frequency;
    expect(frequencyError).to.deep.equal([
      'Frequency can\'t be blank',
    ]);

    resp = await request(app)
      .post('/api/chores')
      .set('Authorization', `Bearer ${token}`)
      .send({ frequency: null });
    expect(resp.status).to.equal(422);
    expect(resp.body.error.frequency).to.not.equal(undefined);
    frequencyError = resp.body.error.frequency;
    expect(frequencyError).to.deep.equal([
      'Frequency can\'t be blank',
    ]);

    resp = await request(app)
      .post('/api/chores')
      .set('Authorization', `Bearer ${token}`)
      .send({ frequency: '' });
    expect(resp.status).to.equal(422);
    expect(resp.body.error.frequency).to.not.equal(undefined);
    frequencyError = resp.body.error.frequency;
    expect(frequencyError).to.deep.equal([
      'Frequency can\'t be blank',
      'Frequency  is not a valid frequency',
    ]);

    resp = await request(app)
      .post('/api/chores')
      .set('Authorization', `Bearer ${token}`)
      .send({ frequency: '   ' });
    expect(resp.status).to.equal(422);
    expect(resp.body.error.frequency).to.not.equal(undefined);
    frequencyError = resp.body.error.frequency;
    expect(frequencyError).to.deep.equal([
      'Frequency can\'t be blank',
      'Frequency     is not a valid frequency',
    ]);

    resp = await request(app)
      .post('/api/chores')
      .set('Authorization', `Bearer ${token}`)
      .send({ frequency: 'some random thing' });
    expect(resp.status).to.equal(422);
    expect(resp.body.error.frequency).to.not.equal(undefined);
    frequencyError = resp.body.error.frequency;
    expect(frequencyError).to.deep.equal([
      'Frequency some random thing is not a valid frequency',
    ]);

    resp = await request(app)
      .post('/api/chores')
      .set('Authorization', `Bearer ${token}`)
      .send({ frequency: 'days' });
    expect(resp.status).to.equal(422);
    expect(resp.body.error.frequency).to.equal(undefined);
  });

  it('does not accept invalid frequencyAmount', async () => {
    let resp = await request(app)
      .post('/api/chores')
      .set('Authorization', `Bearer ${token}`)
      .send({});
    expect(resp.status).to.equal(422);
    expect(resp.body.error.frequencyAmount).to.not.equal(undefined);
    let frequencyAmountError = resp.body.error.frequencyAmount;
    expect(frequencyAmountError).to.deep.equal([
      'Frequency amount can\'t be blank',
    ]);

    resp = await request(app)
      .post('/api/chores')
      .set('Authorization', `Bearer ${token}`)
      .send({ frequencyAmount: null });
    expect(resp.status).to.equal(422);
    expect(resp.body.error.frequencyAmount).to.not.equal(undefined);
    frequencyAmountError = resp.body.error.frequencyAmount;
    expect(frequencyAmountError).to.deep.equal([
      'Frequency amount can\'t be blank',
    ]);

    resp = await request(app)
      .post('/api/chores')
      .set('Authorization', `Bearer ${token}`)
      .send({ frequencyAmount: '' });
    expect(resp.status).to.equal(422);
    expect(resp.body.error.frequencyAmount).to.not.equal(undefined);
    frequencyAmountError = resp.body.error.frequencyAmount;
    expect(frequencyAmountError).to.deep.equal([
      'Frequency amount can\'t be blank',
      'Frequency amount is not a number',
    ]);

    resp = await request(app)
      .post('/api/chores')
      .set('Authorization', `Bearer ${token}`)
      .send({ frequencyAmount: 'some random thing' });
    expect(resp.status).to.equal(422);
    expect(resp.body.error.frequencyAmount).to.not.equal(undefined);
    frequencyAmountError = resp.body.error.frequencyAmount;
    expect(frequencyAmountError).to.deep.equal([
      'Frequency amount is not a number',
    ]);

    resp = await request(app)
      .post('/api/chores')
      .set('Authorization', `Bearer ${token}`)
      .send({ frequencyAmount: 0 });
    expect(resp.status).to.equal(422);
    expect(resp.body.error.frequencyAmount).to.not.equal(undefined);
    frequencyAmountError = resp.body.error.frequencyAmount;
    expect(frequencyAmountError).to.deep.equal([
      'Frequency amount must be greater than 0',
    ]);

    resp = await request(app)
      .post('/api/chores')
      .set('Authorization', `Bearer ${token}`)
      .send({ frequencyAmount: '1' });
    expect(resp.status).to.equal(422);
    expect(resp.body.error.frequencyAmount).to.equal(undefined);

    resp = await request(app)
      .post('/api/chores')
      .set('Authorization', `Bearer ${token}`)
      .send({ frequencyAmount: 1 });
    expect(resp.status).to.equal(422);
    expect(resp.body.error.frequencyAmount).to.equal(undefined);
  });

  it('should create a chore if valid', async () => {
    let count = await orm.Chore.query().count('* as all').first();
    expect(count.all).to.equal(0);
    const resp = await request(app)
      .post('/api/chores')
      .set('Authorization', `Bearer ${token}`)
      .send({
        name: 'Test name',
        frequency: 'days',
        frequencyAmount: 1,
      });
    expect(resp.status).to.equal(201);
    count = await orm.Chore.query().count('* as all').first();
    expect(count.all).to.equal(1);

    const chore = await orm.Chore.query().first();
    expect(resp.body.id).to.equal(chore.id);
    expect(resp.body.name).to.equal('Test name');
    expect(resp.body.frequency).to.equal('days');
    expect(resp.body.frequencyAmount).to.equal(1);
    expect(resp.body.userId).to.equal(user.id);
    expect(chore.id).to.equal(chore.id);
    expect(chore.name).to.equal('Test name');
    expect(chore.frequency).to.equal('days');
    expect(chore.frequencyAmount).to.equal(1);
    expect(chore.userId).to.equal(user.id);
  });

  it('should not create duplicate chores', async () => {
    await user.$relatedQuery('chores')
      .insert({
        name: 'Test name',
        frequency: 'days',
        frequencyAmount: 1,
      });
    const resp = await request(app)
      .post('/api/chores')
      .set('Authorization', `Bearer ${token}`)
      .send({
        name: 'Test name',
        frequency: 'days',
        frequencyAmount: 1,
      });
    expect(resp.status).to.equal(422);
    const count = await orm.Chore.query().count('* as all').first();
    expect(count.all).to.equal(1);

    expect(resp.body).to.deep.equal({
      error: {
        name: ['A chore with that name exists'],
      },
    });
  });

  it('should default to now as start date', async () => {
    let count = await orm.Chore.query().count('* as all').first();
    expect(count.all).to.equal(0);
    const before = new Date();
    const resp = await request(app)
      .post('/api/chores')
      .set('Authorization', `Bearer ${token}`)
      .send({
        name: 'Test name',
        frequency: 'days',
        frequencyAmount: 1,
      });
    const after = new Date();
    expect(resp.status).to.equal(201);
    count = await orm.Chore.query().count('* as all').first();
    expect(count.all).to.equal(1);

    const chore = await orm.Chore.query().first();
    expect(new Date(resp.body.startDate)).to.deep.equal(new Date(chore.startDate));
    expect(isWithinRange(new Date(chore.startDate), before, after));
  });

  it('should use provided start date', async () => {
    let count = await orm.Chore.query().count('* as all').first();
    expect(count.all).to.equal(0);
    const resp = await request(app)
      .post('/api/chores')
      .set('Authorization', `Bearer ${token}`)
      .send({
        name: 'Test name',
        frequency: 'days',
        frequencyAmount: 1,
        startDate: '2018-04-01',
      });
    expect(resp.status).to.equal(201);
    count = await orm.Chore.query().count('* as all').first();
    expect(count.all).to.equal(1);

    const chore = await orm.Chore.query().first();
    expect(new Date(resp.body.startDate)).to.deep.equal(new Date(chore.startDate));
    expect(new Date(chore.startDate)).to.deep.equal(new Date('2018-04-01'));
  });

  it('should create instance', async () => {
    let count = await orm.Chore.query().count('* as all').first();
    expect(count.all).to.equal(0);
    count = await orm.ChoreInstance.query().count('* as all').first();
    expect(count.all).to.equal(0);
    const before = new Date();
    const resp = await request(app)
      .post('/api/chores')
      .set('Authorization', `Bearer ${token}`)
      .send({
        name: 'Test name',
        frequency: 'days',
        frequencyAmount: 1,
        notes: 'foo',
      });
    const after = new Date();
    expect(resp.status).to.equal(201);
    count = await orm.Chore.query().count('* as all').first();
    expect(count.all).to.equal(1);
    count = await orm.ChoreInstance.query().count('* as all').first();
    expect(count.all).to.equal(1);
    const chore = await orm.Chore.query().first();
    const instance = await orm.ChoreInstance.query().first();
    expect(instance.choreId).to.equal(chore.id);
    expect(instance.name).to.equal(chore.name);
    expect(instance.notes).to.equal(chore.notes);
    expect(
      isWithinRange(
        instance.dueDate,
        addDays(before, 1),
        addDays(after, 1),
      )
    ).to.equal(true);
  });

  afterEach(async () => {
    await orm.ChoreInstance.query().delete();
    await orm.Chore.query().delete();
  });

  after(async () => {
    await orm.UserToken.query().delete();
    await orm.User.query().delete();
    db.destroy();
  });
});
