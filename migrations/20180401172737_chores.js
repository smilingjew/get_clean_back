
exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTable('chore', table => {
      table.increments()
        .notNullable();

      table.timestamps(null, true);

      table.string('name', 255)
        .notNullable();

      table.text('notes');

      table.enu('frequency', [
        'days',
        'weeks',
        'months',
        'years',
      ])
        .notNullable();

      table.integer('frequency_amount')
        .unsigned()
        .notNullable();

      table.dateTime('start_date')
        .notNullable();

      table.integer('user_id')
        .unsigned()
        .notNullable();
      table.foreign('user_id')
        .references('user.id')
        .onDelete('CASCADE');
      table.unique(['name', 'user_id']);
    }),
    knex.schema.createTable('chore_instance', table => {
      table.increments()
        .notNullable();

      table.timestamps(null, true);

      table.string('name', 255)
        .notNullable();

      table.text('notes');
      table.text('instance_notes');

      table.dateTime('due_date')
        .notNullable();

      table.dateTime('completed_date');
      table.boolean('completed')
        .notNullable()
        .defaultTo(false);

      table.integer('user_id')
        .unsigned()
        .notNullable();
      table.foreign('user_id')
        .references('user.id')
        .onDelete('CASCADE');

      table.integer('chore_id')
        .unsigned()
        .notNullable();
      table.foreign('chore_id')
        .references('chore.id')
        .onDelete('CASCADE');
    }),
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTable('chore_instance'),
    knex.schema.dropTable('chore'),
  ]);
};
