
exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTable('user', table => {
      table.increments()
        .notNullable();
      table.timestamps(null, true);
      table.string('email', 255)
        .notNullable()
        .unique();
      table.text('password')
        .notNullable();
    }),
    knex.schema.createTable('user_token', table => {
      table.increments()
        .notNullable();
      table.timestamps(null, true);
      table.dateTime('expiration')
        .notNullable();
      table.string('token')
        .notNullable()
        .unique();
      table.integer('user_id')
        .unsigned()
        .notNullable();
      table.foreign('user_id')
        .references('user.id')
        .onDelete('CASCADE');
    }),
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTable('user_token'),
    knex.schema.dropTable('user'),
  ]);
};
