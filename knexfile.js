module.exports = {
  test: {
    client: 'sqlite3',
    useNullAsDefault: true,
    acquireConnectionTimeout: 100,
    connection: {
      filename: ':memory:',
      pool: {
        min: 0,
        max: 2,
        idleTimeoutMillis: 1000,
        afterCreate: async (conn) => {
          await conn.run('PRAGMA foreign_keys = ON');
          return conn;
        },
      },
    },
  },
  development: {
    client: 'sqlite3',
    useNullAsDefault: true,
    connection: {
      filename: './data.db',
      pool: {
        afterCreate: async (conn) => {
          await conn.run('PRAGMA foreign_keys = ON');
          return conn;
        },
      },
    },
  },
};
