const getDatabase = require('../database');

module.exports = {
  getTestDatabase: async () => {
    const db = getDatabase();
    await db.migrate.latest();
    return db;
  },
};
