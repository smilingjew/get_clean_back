const { Model } = require('objection');
const User = require('./user');
const UserToken = require('./userToken');
const Chore = require('./chore');
const ChoreInstance = require('./choreInstance');

module.exports = (db) => {
  Model.knex(db);

  return {
    User,
    UserToken,
    Chore,
    ChoreInstance,
  };
};
