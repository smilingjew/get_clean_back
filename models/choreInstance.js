const BaseModel = require('./base');

class ChoreInstance extends BaseModel {
  static get tableName() {
    return 'chore_instance';
  }

  toJSON(opt) {
    const json = super.toJSON(opt);
    json.dueDate = new Date(json.dueDate);
    if (json.completedDate) {
      json.completedDate = new Date(json.completedDate);
    }
    this.completed == 1 ? json.completed = true : json.completed = false;
    return json;
  }

  async completeInstance(completionDate) {
    const Chore = require('./chore');
    this.completed = true;
    this.completedDate = completionDate;
    await this.$query().update(this).where({ id: this.id });
    const chore = await Chore.query().where({ id: this.choreId }).first();
    await chore.createInstance(completionDate);
  }
}

module.exports = ChoreInstance;
