const {
  addDays,
  addWeeks,
  addMonths,
  addYears,
} = require('date-fns');
const { Model } = require('objection');
const BaseModel = require('./base');

class Chore extends BaseModel {
  static get tableName() {
    return 'chore';
  }

  static get excludedFields() {
    return [
      'choreInstances',
    ];
  }


  static get relationMappings() {
    const ChoreInstance = require('./choreInstance');
    return {
      choreInstances: {
        relation: Model.HasManyRelation,
        modelClass: ChoreInstance,
        join: {
          from: 'chore.id',
          to: 'chore_instance.chore_id',
        },
      },
    };
  }

  $beforeInsert() {
    super.$beforeInsert();
    if (!this.startDate) {
      this.startDate = new Date();
    }
  }

  async createInstance(fromDate) {
    let addFunc;
    switch(this.frequency) {
      case 'days':
        addFunc = addDays;
        break;
      case 'weeks':
        addFunc = addWeeks;
        break;
      case 'months':
        addFunc = addMonths;
        break;
      case 'years':
        addFunc = addYears;
        break;
      default:
        throw new Error('Unkown Frequency');
    }
    const dueDate = addFunc(fromDate, this.frequencyAmount);
    await this.$relatedQuery('choreInstances')
      .insert({
        dueDate,
        name: this.name,
        notes: this.notes,
        userId: this.userId,
        completed: false,
      });
  }

  async $afterInsert() {
    await super.$afterInsert();
    await this.createInstance(this.startDate);
  }

  toJSON(opt) {
    const json = super.toJSON(opt);
    json.startDate = new Date(json.startDate);
    return json;
  }


  static get constraints() {
    return {
      name: {
        presence: {
          allowEmpty: false,
        },
      },
      frequency: {
        presence: {
          allowEmpty: false,
        },
        inclusion: {
          within: [
            'days',
            'weeks',
            'months',
            'years',
          ],
          message: '%{value} is not a valid frequency',
        },
      },
      frequencyAmount: {
        presence: {
          allowEmpty: false,
        },
        numericality: {
          onlyInteger: true,
          greaterThan: 0,
        },
      },
      startDate: {
        datetime: true,
      },
    };
  }
}

module.exports = Chore;
