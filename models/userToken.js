const { v4 } = require('uuid');
const { addHours } = require('date-fns');
const BaseModel = require('./base');

class UserToken extends BaseModel {
  static get tableName() {
    return 'user_token';
  }

  static getToken() {
    return v4();
  }

  $beforeInsert() {
    super.$beforeInsert();
    if (!this.token ) {
      this.token = this.constructor.getToken();
    }
    if (!this.expiration) {
      this.expiration = addHours(new Date(), 1);
    }
  }
}

module.exports = UserToken;
