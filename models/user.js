const bcrypt = require('bcrypt');
const { Model } = require('objection');
const BaseModel = require('./base');

class User extends BaseModel {
  static get tableName() {
    return 'user';
  }

  static get excludedFields() {
    return [
      'password',
      'createdAt',
      'updatedAt',
      'tokens',
      'chores',
      'choreInstances',
    ];
  }

  static get relationMappings() {
    const UserToken = require('./userToken');
    const Chore = require('./chore');
    const ChoreInstance = require('./choreInstance');
    return {
      tokens: {
        relation: Model.HasManyRelation,
        modelClass: UserToken,
        join: {
          from: 'user.id',
          to: 'user_token.user_id',
        },
      },
      chores: {
        relation: Model.HasManyRelation,
        modelClass: Chore,
        join: {
          from: 'user.id',
          to: 'chore.user_id',
        },
      },
      choreInstances: {
        relation: Model.HasManyRelation,
        modelClass: ChoreInstance,
        join: {
          from: 'user.id',
          to: 'chore_instance.user_id',
        },
      },
    };
  }

  static async hashPassword(password) {
    const hash = await bcrypt.hash(password, 10);
    return hash;
  }

  static async getUserFromAuth(authHeader) {
    if (!authHeader) {
      return null;
    }
    if(authHeader.indexOf('Bearer ') === -1) {
      return null;
    }
    const tokenString = authHeader.substring(7);
    if (tokenString.trim() === '') {
      return null;
    }
    const UserToken = require('./userToken');
    const token = await UserToken.query()
      .where({ token: tokenString })
      .where('expiration', '>', new Date())
      .first();
    if (!token) {
      return null;
    }
    const user = await User.query()
      .where({ id: token.userId })
      .first();
    if (!user) {
      return null;
    }
    return user;
  }

  static get constraints() {
    return {
      email: {
        presence: {
          allowEmpty: false,
        },
      },
      password: {
        presence: {
          allowEmpty: false,
        },
      },
    };
  }

  async checkPassword(password) {
    const valid = await bcrypt.compare(password, this.password);
    return valid;
  }

  async getToken() {
    let token = await this.$relatedQuery('tokens')
      .where('expiration', '>', new Date().getTime())
      .first();
    if(token) {
      return token.token;
    }
    token = await this.$relatedQuery('tokens').insert({});
    return token.token;
  }
}

module.exports = User;
