const { Model, snakeCaseMappers, lodash: _ } = require('objection');
const { Validator } = require('../validation');

class BaseModel extends Model {
  $beforeInsert() {
    this.createdAt = new Date();
    this.updatedAt = new Date();
    return;
  }

  toJSON(opt) {
    let json = super.toJSON(opt);
    if (this.constructor.excludedFields) {
      json = _.omit(json, this.constructor.excludedFields);
    }
    json.createdAt = new Date(json.createdAt);
    json.updatedAt = new Date(json.updatedAt);
    return json;
  }

  $beforeUpdate() {
    this.updatedAt = new Date();
    return;
  }

  static get columnNameMappers() {
    return snakeCaseMappers();
  }

  static createValidator() {
    return new Validator();
  }
}

module.exports = BaseModel;
